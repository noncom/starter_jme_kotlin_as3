package org.hatchery.noncom.lll17

import com.jme3.app.SimpleApplication
import com.jme3.material.Material
import com.jme3.math.ColorRGBA
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Box

/**
 * Created by noncom on 10.07.2017.
 */

class LLL17 : SimpleApplication() {
    override fun simpleInitApp() {
        val box = Box(1f, 1f, 1f)
        val geom = Geometry("Box", box)

        val material = Material(getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md")
        material.setColor("Color", ColorRGBA.Blue)
        geom.material = material

        getRootNode().attachChild(geom)
    }
}