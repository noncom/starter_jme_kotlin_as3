package org.hatchery.noncom.lll17

/**
 * Created by noncom on 10.07.2017.
 */

class DesktopLauncher {

    fun main(args: Array<String>) {
        val game = LLL17()
        game.start()
    }

}